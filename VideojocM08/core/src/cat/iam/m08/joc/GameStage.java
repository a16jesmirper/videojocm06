package cat.iam.m08.joc;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;

public class GameStage extends Stage implements ContactListener{

	private static final int VIEWPORT_WIDTH = 20;
	private static final int VIEWPORT_HEIGHT = 13;

	private World world;
	private Ground ground;
	private Bird bird;

	private final float TIME_STEP = 1 / 300f;
	private float accumulator = 0f;

	private OrthographicCamera camera;
	private Box2DDebugRenderer renderer;
	
	private Rectangle screenRightSide;
	private Rectangle screenLeftSide;

    private Vector3 touchPoint;

	public GameStage() {
		setUpWorld();
		setupCamera();
		setupTouchControlAreas();
		renderer = new Box2DDebugRenderer();
		
	}

	private void setUpWorld() {
		world = WorldUtils.createWorld();
		world.setContactListener(this);
		setUpGround();
		setUpBird();
		createEnemy();
	}

	private void setUpGround() {
		ground = new Ground(WorldUtils.createGround(world));
		addActor(ground);
	}

	private void setUpBird() {
		bird = new Bird(WorldUtils.createBird(world));
		
		addActor(bird);
	}

	private void setupCamera() {
		camera = new OrthographicCamera(VIEWPORT_WIDTH, VIEWPORT_HEIGHT);
		camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0f);
		camera.update();
	}
	
	private void setupTouchControlAreas() {
        touchPoint = new Vector3();
        screenLeftSide = new Rectangle(0, 0, getCamera().viewportWidth / 2, getCamera().viewportHeight);
        screenRightSide = new Rectangle(getCamera().viewportWidth / 2, 0, getCamera().viewportWidth / 2,
                getCamera().viewportHeight);
        Gdx.input.setInputProcessor(this);
	}


	@Override
	public void act(float delta) {
		super.act(delta);
		Array<Body> bodies = new Array<Body>(world.getBodyCount());
        world.getBodies(bodies);

        for (Body body : bodies) {
            update(body);
        }

		// Fixed timestep
		accumulator += delta;

		while (accumulator >= delta) {
			world.step(TIME_STEP, 6, 2);
			accumulator -= TIME_STEP;
		}

		// TODO: Implement interpolation

	}

	@Override
	public void draw() {
		super.draw();
		renderer.render(world, camera.combined);
	}
	
	private void update(Body body) {
		
		bird.jumpingStart();
        if (!BodyUtils.bodyInBounds(body)) {
            if (BodyUtils.bodyIsEnemy(body)) {
                createEnemy();
            }
            world.destroyBody(body);
        }
    }

    private void createEnemy() {
        Enemy enemy = new Enemy(WorldUtils.createEnemy(world));
        addActor(enemy);
    }
	
	//se llama cuando se presiona una tecla
	
	@Override
	public boolean keyDown (int keycode) {
		
		switch (keycode)
	    {
		case Keys.SPACE:
			bird.jump();
			break;
		
	    }
	    return true;
	   }
	
	
	//se llama cuando se levanta una tecla
	
	@Override
    public boolean keyUp(int keycode)
    {
	    switch (keycode)
	    {
		case Keys.SPACE:
			//bob.setLeftMove(false);
			break;
	    }
	    return true;
    }
	


    /**
     * Helper function to get the actual coordinates in my world
     * @param x
     * @param y
     */
    

	@Override
	public void beginContact(Contact contact) {
		Body a = contact.getFixtureA().getBody();
        Body b = contact.getFixtureB().getBody();

        
		
	}

	@Override
	public void endContact(Contact contact) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		// TODO Auto-generated method stub
		
	}

}
