package cat.iam.m08.joc;

import com.badlogic.gdx.math.Vector2;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;


/* Clase para crear los diferentes bodys 
 * que incorporaremos en el world
 */
public class WorldUtils {
	
	public static float OFFSET = Constants.GROUND_WIDTH / 2;

	//crear world
	public static World createWorld() {
        return new World(Constants.WORLD_GRAVITY, true);
    }

	//crear el terra
    public static Body createGround(World world) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(new Vector2(Constants.GROUND_X + OFFSET, Constants.GROUND_Y));
        Body body = world.createBody(bodyDef);
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(Constants.GROUND_WIDTH / 2, Constants.GROUND_HEIGHT / 2);
        body.createFixture(shape, Constants.GROUND_DENSITY); 
        body.setUserData(new GroundUserData());
        shape.dispose();
        return body;
    }
    
    //crear personatge bird
    public static Body createBird(World world) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(new Vector2(Constants.BIRD_X, Constants.BIRD_Y));
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(Constants.BIRD_WIDTH / 2, Constants.BIRD_HEIGHT / 2);
        Body body = world.createBody(bodyDef);
        body.setGravityScale(Constants.BIRD_GRAVITY_SCALE);
        body.createFixture(shape, Constants.BIRD_DENSITY);
        body.resetMassData();
        body.setUserData(new BirdUserData(Constants.BIRD_WIDTH, Constants.BIRD_HEIGHT));
        shape.dispose();
        return body;
    
    }
    
    //crear enemics
    public static Body createEnemy(World world) {
        EnemyType enemyType = RandomUtils.getRandomEnemyType();
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.KinematicBody;
        bodyDef.position.set(new Vector2(enemyType.getX(), enemyType.getY()));
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(enemyType.getWidth() / 2, enemyType.getHeight() / 2);
        Body body = world.createBody(bodyDef);
        body.createFixture(shape, enemyType.getDensity());
        body.resetMassData();
        EnemyUserData userData = new EnemyUserData(enemyType.getWidth(), enemyType.getHeight());
        body.setUserData(userData);
        shape.dispose();
        return body;
    }


}
