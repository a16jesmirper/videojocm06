package cat.iam.m08.joc;

import com.badlogic.gdx.physics.box2d.Body;

public class Bird extends GameActor{
	private boolean jumping;
	private boolean start = true;
	private boolean dodging;


    public Bird(Body body) {
        super(body);
    }

    @Override
    public BirdUserData getUserData() {
        return (BirdUserData) userData;
    }

    //metode per saltar el bird
    public void jump() {

        if (!jumping || jumping) {
            body.applyLinearImpulse(getUserData().getJumpingLinearImpulse(), body.getWorldCenter(), true);
           
            jumping = true;
            start = false;
        }

    }
    
    public void jumpingStart() {
    	
    	System.out.println("hola");
    		body.applyLinearImpulse(getUserData().getStartLinearImpulse(), body.getWorldCenter(), true);
    		body.applyLinearImpulse(getUserData().getStartLinearImpulseOposite(), body.getWorldCenter(), true);
    		
    	
    	
    }
/*
    public void landed() {
        jumping = false;
    }
    
    public void dodge() {
        if (!jumping) {
            body.setTransform(getUserData().getDodgePosition(), getUserData().getDodgeAngle());
            dodging = true;
        }
    }

    public void stopDodge() {
        dodging = false;
        body.setTransform(getUserData().getRunningPosition(), 0f);
    }

    public boolean isDodging() {
        return dodging;
    }

*/
}
