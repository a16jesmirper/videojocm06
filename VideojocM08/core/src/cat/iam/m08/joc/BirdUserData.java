package cat.iam.m08.joc;

import com.badlogic.gdx.math.Vector2;

public class BirdUserData extends UserData{
	
	//En esta clase almacenamos insformacion sobre el movimiento de bird
	
	private final Vector2 runningPosition = new Vector2(Constants.BIRD_X, Constants.BIRD_Y);
    private final Vector2 dodgePosition = new Vector2(Constants.BIRD_DODGE_X, Constants.BIRD_DODGE_Y);
	private Vector2 jumpingLinearImpulse;
	private Vector2 startLinearImpulse;
	private Vector2 startLinearImpulseOposite;
	

	public BirdUserData(float width, float height) {
        super(width, height);
        jumpingLinearImpulse = Constants.BIRD_JUMPING_LINEAR_IMPULSE;
        startLinearImpulse = Constants.BIRD_START_LINEAR_IMPULSE;
        userDataType = UserDataType.RUNNER;
        startLinearImpulseOposite = Constants.BIRD_START_LINEAR_IMPULSE_OPOSITE;
    }

    public Vector2 getJumpingLinearImpulse() {
        return jumpingLinearImpulse;
    }
    
    public Vector2 getStartLinearImpulse() {
    	return startLinearImpulse;
    }
    
    public Vector2 getStartLinearImpulseOposite() {
    	return startLinearImpulseOposite;
    }

    public void setJumpingLinearImpulse(Vector2 jumpingLinearImpulse) {
        this.jumpingLinearImpulse = jumpingLinearImpulse;
    }
    

}
