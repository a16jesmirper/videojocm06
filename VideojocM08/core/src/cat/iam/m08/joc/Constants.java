package cat.iam.m08.joc;

import com.badlogic.gdx.math.Vector2;

public class Constants {

	public static final int APP_WIDTH = 800;
    public static final int APP_HEIGHT = 480;

    public static final Vector2 WORLD_GRAVITY = new Vector2(0, -10);

    public static final float GROUND_X = 0;
    public static final float GROUND_Y = 0;
    public static final float GROUND_WIDTH = 25f;
    public static final float GROUND_HEIGHT = 2f;
    public static final float GROUND_DENSITY = 0f;
    
  //posicion del personaje en el world
    public static final float BIRD_X = 6; 
    public static final float BIRD_Y = GROUND_Y + GROUND_HEIGHT;
    public static final float BIRD_WIDTH = 1f;
    public static final float BIRD_HEIGHT = 1f;
    
    public static final float BIRD_GRAVITY_SCALE = 3f; //gravedad aplicada al bajar
    public static float BIRD_DENSITY = 0.5f;
    public static final float BIRD_DODGE_X = 2f;
    public static final float BIRD_DODGE_Y = 1.5f;
    public static final Vector2 BIRD_JUMPING_LINEAR_IMPULSE = new Vector2(0f, 10f); //vector2(x, y) impulso al apretar teclar space
    public static final Vector2 BIRD_START_LINEAR_IMPULSE = new Vector2(0f, 5f);
    public static final Vector2 BIRD_START_LINEAR_IMPULSE_OPOSITE = new Vector2(0f, -3f);
    
    
    public static final float ENEMY_X = 25f;
    public static final float ENEMY_DENSITY = BIRD_DENSITY;
    public static final float FLYING_ENEMY_Y = 3f;
    public static final Vector2 ENEMY_LINEAR_VELOCITY = new Vector2(-10f, 0);
	



}
